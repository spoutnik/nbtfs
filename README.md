# NBTFS

### Goal
This project is destined to translate a Minecraft save file into a filesystem that can be mounted and used with UNIX tools, using `Fuse` capabilities.

### How it works
A big problem I encountered while playing Minecraft was save data corruption between versions. Minecraft uses a custom save format, that can be described as a binary json, with multiple objects defined within. This format is very hard to edit and interpret.

By parsing and separating each object into either files or directories, I thought of exposing it to the user as a filesystem, allowing to search and modify it easily, then compiling it back into the binary format when unmounted.
