SRC = src
BIN = bin
CXXFLAGS = -g -D_FILE_OFFSET_BITS=64 -std=c++11
DEFAULT_FLAGS = -D_FILE_OFFSET_BITS=64 -std=c++11
LIBS = -lz
CC = g++

default: $(BIN)/nbt.o $(BIN)/helper.o $(BIN)/nbtfs.o
	$(CXX) $(FLAGS) $(LIBS) -o $(BIN)/reader $^ $(SRC)/main.cpp

nbttojson: $(BIN)/nbt.o $(BIN)/helper.o
	$(CXX) $(DEFAULT_FLAGS) $(LIBS) -o $(BIN)/$@ $^ $(SRC)/$@.cpp

blocks: $(BIN)/nbt.o $(BIN)/helper.o
	$(CXX) $(DEFAULT_FLAGS) $(LIBS) -o $(BIN)/$@ $^ $(SRC)/$@.cpp

clean:
	rm $(BIN)/*

$(BIN)/%.o: $(SRC)/%.cpp
	$(CXX) $(CXXFLAGS) -c -o $@ $<
