#include <fuse.h>
#include <string>

#include "nbt.h"

class NBTfs {
    private:
	class file {
	    public:
		file(string name);
		file(string name, off_t begin, size_t size);

		string name;
		char contents[4096];
		int size;
	};

	class directory: public file {
	    public:
		directory(string name);

		std::map<string, file&> contents;

		void insert(directory dir);
		void insert(file& f);
	};

	struct fuse_operations nbtfs_opers;
	NBT* data;

	void* simple_init();
	int simple_getattr(const char *path, struct stat *stbuf);
	int simple_getdir(const char *path, fuse_dirh_t h, fuse_dirfil_t filler);
	int simple_open(const char *path, int flags);
	int simple_read(const char *path, char *buf, size_t size, off_t offset);

	void translateNBT(NBT* data);

    public:
	NBTfs();
	NBTfs(NBT* data);
};

//static struct fuse_operations simple_oper = {
    //.getattr	= NBTfs::simple_getattr,
    //.getdir	= NBTfs::simple_getdir,
    //.open	= NBTfs::simple_open,
    //.read	= NBTfs::simple_read,
    //	.mkdir		=
    //	.unlink		=
    // 	.rmdir		=
    //	.rename		=
    //	.chmod		=
//};
