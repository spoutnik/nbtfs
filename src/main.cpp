#include <stdio.h>
#include <string.h>
#include "nbt.h"
#include "nbtfs.h"

int main(int argc, char** argv) {
	if (argc != 2) {
		return 1;
	}

	bool result;
	NBT* import = new NBT(argv[1], result);
	import->printTags();

	delete import;
}
