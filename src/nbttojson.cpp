#include <cstdio>
#include "nbt.h"

int main(int argc, char** argv) {
	if (argc != 2) {
		printf("Usage: %s <NBT file>\n", argv[0]);
		return 1;
	}

	bool result;
	NBT root(argv[1], result);

	if (result)
		printf("%s\n", root.toJson().dump().c_str());
}
